import React from "react";
import { Route } from "react-router-dom";
import Template from "./AppTemplate";

export default class TemplatedRoute extends React.Component {
    render() {
        let { component: Component, render: Render, ...other } = this.props;

        if(Render) {
            return (
                <Route {...other} component={props => (
                    <Template>
                        <Render {...props} />
                    </Template>
                )} />
            )
        } else {
            return (
                <Route {...other} component={props => (
                    <Template>
                        <Component {...props} />
                    </Template>
                )} />
            )
        }
    }
}