import EasyMDE from "easymde/src/js/easymde";

export default function() {
    return [
        { name: "bold", action: EasyMDE.toggleBold, className: "fas fa-bold", title: "Bold" },
        { name: "italic", action: EasyMDE.toggleItalic, className: "fas fa-italic", title: "Italic" },
        { name: "strikethrough", action: EasyMDE.toggleStrikethrough, className: "fas fa-strikethrough", title: "Strikethrough" },
        { name: "heading", action: EasyMDE.toggleHeadingSmaller, className: "fas fa-heading", title: "Heading" },
        "|",
        { name: "code", action: EasyMDE.toggleCodeBlock, className: "fas fa-code", title: "Code Block" },
        { name: "quote", action: EasyMDE.toggleBlockquote, className: "fas fa-quote-right", title: "Blockquote" },
        "|",
        { name: "ordered-list", action: EasyMDE.toggleOrderedList, className: "fas fa-list-ol", title: "Ordered List" },
        { name: "unordered-list", action: EasyMDE.toggleUnorderedList, className: "fas fa-list-ul", title: "Unordered List" },
        "|",
        { name: "link", action: EasyMDE.drawLink, className: "fas fa-link", title: "Create Link" },
        { name: "image", action: EasyMDE.drawImage, className: "far fa-image", title: "Insert Image" },
        { name: "emojis", action: this.showEmojiPicker, className: "far fa-smile", title: "Insert Emojis" },
        { name: "horizontal-rule", action: EasyMDE.drawHorizontalRule, className: "fas fa-minus", title: "Insert Horizontal Line" },
        "|",
        { name: "fullscreen", action: EasyMDE.toggleFullScreen, className: "fas fa-expand", noDisable: true, noMobile: true, title: "Toggle Fullscreen" },
        { name: "preview", action: EasyMDE.togglePreview, className: "fas fa-eye", noDisable: true, title: "Toggle Preview" },
        { name: "side-by-side", action: EasyMDE.toggleSideBySide, className: "fas fa-columns", noDisable: true, noMobile: true, title: "Toggle Side-by-Side" }
    ]
}