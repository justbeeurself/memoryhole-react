import React, { Component } from "react";
import { AutoRotatingCarousel, Slide } from "material-auto-rotating-carousel";
import { withSettingsAccess } from "~/hoc/SettingsInjector";
import { isResponsive } from "~/hoc/Responsive";
import { withStyles, classNames } from "~/hoc/StyleComposer";
import Zoom from "@material-ui/core/Zoom";
import purple from "@material-ui/core/colors/purple";
import blueGrey from "@material-ui/core/colors/blueGrey";
import green from "@material-ui/core/colors/green";
import teal from "@material-ui/core/colors/teal";
import brown from "@material-ui/core/colors/brown";

import Icon from "@material-ui/core/Icon";

import style from "./style";

const StyledIcon = withStyles(style)(({ classes, i }) => (
    <Icon className={classes.icon}>{i}</Icon>
))

@isResponsive
@withSettingsAccess
@withStyles(style)
export default class Introduction extends Component {
    state = {
        open: this.props.settings.getValue("showHelp")
    }

    onClose = () => {
        let { settings } = this.props;
        this.setState({ open: false }, () => settings.setValue("showHelp", false));
    }

    render() {
        let { settings, classes, isMobile } = this.props;
        if (!settings.getValue("showHelp", true)) return null;

        return (
            <div className={classes.container}>
                <Zoom in={this.state.open}>
                    <div>
                        <AutoRotatingCarousel
                                autoplay={false}
                                mobile={isMobile}
                                label="Okay!"
                                open={this.state.open}
                                onClose={this.onClose} onStart={this.onClose}
                                className={classNames(classes.carousel, isMobile && classes.mobileContainer)}>
                            <Slide
                                media={<StyledIcon i="trip_origin" />}
                                mediaBackgroundStyle={{ backgroundColor: purple[400] }}
                                style={{ backgroundColor: purple[600] }}
                                title="Welcome to The Memory Hole!"
                                subtitle="A new take on an old concept. After 24 hours, anything you post will be wiped forever from our database." />
                            <Slide
                                media={<StyledIcon i="cloud" />}
                                mediaBackgroundStyle={{ backgroundColor: blueGrey[400] }}
                                style={{ backgroundColor: blueGrey[600] }}
                                title="Share anything!"
                                subtitle="Anyone can post Markdown-formatted content on this website, no accounts required, and I am physically incapable of dedicating the manpower to monitor posted content. It's the Wild, Wild West here." />
                            <Slide
                                media={<StyledIcon i="visibility_off" />}
                                mediaBackgroundStyle={{ backgroundColor: green[600] }}
                                style={{ backgroundColor: green[800] }}
                                title="Remain anonymous!"
                                subtitle="Data storage is expensive, and I am by no means wealthy. All non-essential data is wiped from my systems as soon as possible, and I keep no logs." />
                            <Slide
                                media={<StyledIcon i="public" />}
                                mediaBackgroundStyle={{ backgroundColor: brown[400] }}
                                style={{ backgroundColor: brown[600] }}
                                title="People you can trust!"
                                subtitle="Every byte of code is available online for you to scrutinize, or if you feel so inclined, download it and run your own copy! My code is covered by the MIT license." />
                            <Slide
                                media={<StyledIcon i="add_comment" />}
                                mediaBackgroundStyle={{ backgroundColor: teal[400] }}
                                style={{ backgroundColor: teal[600] }}
                                title="Get Started!"
                                subtitle="Read through the Markdown Guide if you are unfamiliar, or jump in and try posting something. If you have any questions, feel free to contact me." />
                        </AutoRotatingCarousel>
                    </div>
                </Zoom>
            </div>
        );
    }
}