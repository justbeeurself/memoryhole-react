import React from "react";
import Typography from "@material-ui/core/Typography";
import Markdown from "~/components/Markdown";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import style from "./style";
import * as DateUtil from "~/services/ui/DateUtil";
import { withPostTheme } from "../Themable";
import { Link } from "react-router-dom";

@withPostTheme(style)
export default class Post extends React.Component {
    state = {
        Markdown: "Loading..."
    };

    get identity() {
        let { post, classes } = this.props;

        if(post.identity && post.identity.length) {
            return (
                <Link to={`/search?identity=${encodeURIComponent(post.identity)}`} className={classes.themedText}>{post.vanityIdentity || post.identity}</Link>
            );
        } else return "Anonymous";
    }

    get createDate() {
        let { post } = this.props;
        return DateUtil.humanizeDate(post.createDate);
    }

    componentDidMount() {
        let { classes, post } = this.props;

        let component = (
            <Markdown content={post.content} className={classes.markdown} />
        );

        this.updateTimer = setInterval(() => {
            this.setState({ lastUpdate: +new Date() })
        }, 5000);

        this.setState({ Markdown: component });
    }

    componentWillUnmount() {
        clearInterval(this.updateTimer);
    }

    render() {
        let { Markdown } = this.state;
        let { post, classes } = this.props;
        let postSize = !!post.narrow ? 6 : 8;

        return (
            <div className={classes.view}>
                <Grid container spacing={0}>
                    <Grid item xs={12} className={classes.wrapper}>
                        <Grid item lg={postSize} md={10} sm={12} xs={12}>
                            <Paper className={classes.paper}>
                                <div className={classes.breadcrumb}>
                                    <Typography component={Link} to="/" variant="body1">Home</Typography>
                                    <Typography variant="body1" className={classes.breadcrumbSeparator}>/</Typography>
                                    {!!post.identity && (
                                        <React.Fragment>
                                            <Typography component={Link}
                                                to={`/search?identity=${encodeURIComponent(post.identity)}`}
                                                variant="body1">{post.vanityIdentity || post.identity}</Typography>
                                            <Typography variant="body1" className={classes.breadcrumbSeparator}>/</Typography>
                                        </React.Fragment>
                                    )}
                                    <Typography variant="body1">{post.uid}</Typography>
                                </div>
                                <Typography variant="display1" className={classes.themedText}>{post.title}</Typography>
                                <Typography variant="body1" className={classes.themedText}>Created by {this.identity} {this.createDate}</Typography>
                            </Paper>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} className={classes.wrapper}>
                        <Grid item lg={postSize} md={10} sm={12} xs={12}>
                            <Paper className={classes.paper}>
                                {Markdown}
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}