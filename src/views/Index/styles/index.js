export default theme => ({
    topCard: {
        display: "flex",
        justifyContent: "center"
    },
    paper: {
        padding: "1rem",

        "@media (max-width: 959px)": {
            padding: "0"
        }
    },
    closeButton: {
        margin: theme.spacing.unit,
        float: "right"
    }
});