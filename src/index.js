import React from "react";
import ReactDOM from "react-dom";
import "./lib/shims";
import "./assets";
import "./settings";
import App, { ErrorBoundary } from "./app";
import { SnackbarProvider } from "material-ui-snackbar-provider";
import withAppTheme from "~/hoc/withAppTheme";
import Introduction from "./components/Introduction";
import UpdateNotifier from "./components/ServiceWorker";

const ThemedApp = withAppTheme(App);

// Catches all uncaught exceptions
let Application = (
    <ErrorBoundary>
        <SnackbarProvider snackbarProps={{ autoHideDuration: 4000 }}>
            <UpdateNotifier />
            <ThemedApp />
        </SnackbarProvider>
        <Introduction />
    </ErrorBoundary>
);

ReactDOM.render(Application, document.getElementById("root"));