import React from "react";
import AsyncComponent from "~/components/AsyncComponent";
import { withStyles } from "~/hoc/StyleComposer";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import colorHelper from "color";
import ColorModal from "./components/ColorPickerModal";

import style from "./style";

@withStyles(style)
export default class ColorPicker extends AsyncComponent {
    state = {
        color: this.props.color || "#fafafa",
        expanded: false
    }

    onClickButton = e => this.setState({ expanded: !this.state.expanded });
    onSelectColor = async(color) => {
        let { onChange } = this.props;
        await this.updateState({ expanded: false, color: !!color ? color : this.state.color });

        if(!!color && onChange) {
            onChange(color);
        }
    }

    static getDerivedStateFromProps(props, state) {
        if(props.color !== state.color) {
            return { color: props.color };
        } else return null;
    }

    render() {
        let { classes, icon = "opacity", label, name, ButtonProps = {}, style: buttonStyle = {} } = this.props;
        let { expanded, color } = this.state;

        let style = {
            ...buttonStyle,
            backgroundColor: color,
            color: colorHelper(color).isDark() ? "#fff" : "#000"
        }

        return (
            <div className={classes.wrapper}>
                <input type="hidden" value={this.state.color} name={name} />
                <Button variant={label ? "extendedFab" : "fab"} className={classes.button} style={style} {...ButtonProps} onClick={this.onClickButton}>
                    <Icon>{icon}</Icon>
                    {label}
                </Button>
                <ColorModal open={expanded} onClose={this.onSelectColor} selectedColor={this.state.color} />
            </div>
        )
    }
}