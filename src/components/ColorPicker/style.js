export default {
    wrapper: {
        margin: ".5rem .25rem",
        display: "inline-block",

        "@media (max-width: 768px)": {
            width: "100%"
        },

        "@media (min-width: 769px) and (max-width: 1024px)": {
            width: "50%",
            marginLeft: 0,
            marginRight: 0,
            paddingLeft: ".5rem",
            paddingRight: ".5rem"
        }
    },

    button: {
        transition: "background-color .4s ease-in-out",
        width: "100%"
    }
}