import React from "react";
import Grid from "@material-ui/core/Grid";

export const isGrid = (options = {}) => Component => props => (
    <Grid container {...options}>
        <Component {...props} />
    </Grid>
);