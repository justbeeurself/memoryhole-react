import React from "react";
import AsyncComponent from "~/components/AsyncComponent";
import { Formik } from "formik";
import config from "config";
import settings from "~/services/settings";
import { withStyles, classNames } from "~/hoc/StyleComposer";
import TextField from "@material-ui/core/TextField";
import PasswordField from "~/components/form/PasswordField";
import FormHelperText from "@material-ui/core/FormHelperText";
import Button from "@material-ui/core/Button";
import Recaptcha from "~/components/form/Recaptcha";
import MarkdownEditor from "~/components/form/MarkdownEditor";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import { withSnackbar } from "material-ui-snackbar-provider";
import Icon from "@material-ui/core/Icon";
import ColorPicker from "~/components/ColorPicker";
import { DEFAULT_COLORS } from "memoryhole-common/dist/colors";
import formSpec from "memoryhole-common/dist/spec/v1/post";

import API from "~/services/api/ApiService";

import style from "./styles";

const getColor = type => settings.getValue(`defaultColors.${type}`, DEFAULT_COLORS[type]);

const Field = withStyles(style)(({ classes, style, children, className }) => (
    <div className={classNames(classes.field, className)} style={style}>
        {children}
    </div>
));

const InnerForm = withStyles(style)(({
    classes,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setFieldValue,
    isReadyToPost,
    isSaving
}) => {
    let basicProps = name => ({
        name,
        value: values[name],
        onChange: handleChange,
        onBlur: handleBlur,
        error: (touched[name] && errors[name] != null)
    });

    return (
        <form autoComplete="off" onSubmit={handleSubmit}>
            <Field className={classes.titleWrapper}>
                <TextField fullWidth
                    label="Post Title"
                    margin="normal"
                    inputProps={{className: classes.titleInput}}
                    {...basicProps("title")} />
                {touched.title && errors.title && (
                    <FormHelperText error className={classes.titleError}>{errors.title}</FormHelperText>
                )}
            </Field>
            <Field>
                <MarkdownEditor {...basicProps("content")} />
                {touched.content && errors.content && (
                    <FormHelperText error className="help">{errors.content}</FormHelperText>
                )}
            </Field>
            <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Optional Fields</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.content}>
                    <Field>
                        <PasswordField showLabel="Identity" fullWidth type="password" {...basicProps("identity")} />
                        {touched.identity && errors.identity && (
                            <FormHelperText error className="help">{errors.identity}</FormHelperText>
                        )}
                    </Field>
                    <Field style={{marginTop: "1rem"}}>
                        <ColorPicker label="Page Foreground" color={values.foregroundColor} onChange={e => setFieldValue("foregroundColor", e)} />
                        <ColorPicker label="Page Background" color={values.backgroundColor} onChange={e => setFieldValue("backgroundColor", e)} />
                        <ColorPicker label="Text Color" color={values.textColor} onChange={e => setFieldValue("textColor", e)} />
                        <ColorPicker label="Link Color" color={values.linkColor} onChange={e => setFieldValue("linkColor", e)} />
                        <ColorPicker label="Quote Color" color={values.quoteColor} onChange={e => setFieldValue("quoteColor", e)} />
                        <ColorPicker label="Code Color" color={values.codeColor} onChange={e => setFieldValue("codeColor", e)} />
                        <ColorPicker label="Accent Color" color={values.accentColor} onChange={e => setFieldValue("accentColor", e)} />
                    </Field>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <Field label="Recaptcha">
                <Recaptcha
                    onVerify={token => setFieldValue("recaptcha", token)}
                    onExpire={() => setFieldValue("recaptcha", "")}
                    sitekey={config.recaptchaKey} />
                {touched.recaptcha && errors.recaptcha && (
                    <FormHelperText error className="help">{errors.recaptcha}</FormHelperText>
                )}
            </Field>
            <Button type="submit" variant="contained" size="large" color="secondary" disabled={!isReadyToPost(values)} className={classes.submitButton}>
                Create Post
                <Icon className={classNames(classes.rightIcon, isSaving && classes.loop)}>{isSaving ? "loop" : "publish"}</Icon>
            </Button>
        </form>
    );
});

@withSnackbar()
@withStyles(style)
export default class IndexForm extends AsyncComponent {
    state = {
        isSaving: false
    }

    initialValues = () => ({
        title: "",
        content: "",
        recaptcha: "",
        narrow: false,
        identity: settings.getValue("defaultIdentity", ""),
        foregroundColor: getColor("foreground"),
        backgroundColor: getColor("background"),
        textColor: getColor("text"),
        linkColor: getColor("link"),
        quoteColor: getColor("quote"),
        codeColor: getColor("code"),
        accentColor: getColor("accent")
    })

    isReadyToPost = values => {
        try {
            formSpec.validateSync(values);
            return true;
        } catch(ex) {
            // console.log(ex);
            return false;
        }
    }

    renderForm = formikProps => (
        <InnerForm {...formikProps}
            isReadyToPost={this.isReadyToPost}
            isSaving={this.state.isSaving} />
    )

    async onSubmit(values) {
        try {
            await this.updateState({ isSaving: true });
            let req = await API.createPost(values);
            if(req.error) throw new Error(req.error);

            await this.updateState({ isSaving: false });
            this.props.history.push(`/p/${req.payload}`);
        } catch(ex) {
            console.log(ex);
            await this.updateState({ isSaving: false });
            this.props.snackbar.showMessage(
                "An error has occurred while creating the post",
                "OK", () => null
            );
        }
    }

    render() {
        return (
            <Formik
                render={this.renderForm}
                validationSchema={formSpec}
                initialValues={this.initialValues()}
                onSubmit={values => this.onSubmit(values)} />
        );
    }
}