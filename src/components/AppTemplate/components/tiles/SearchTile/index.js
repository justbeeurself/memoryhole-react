import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import Popover from "@material-ui/core/Popover";
import RootRef from "@material-ui/core/RootRef";
import Zoom from "@material-ui/core/Zoom";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Tooltip from "@atlaskit/tooltip";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";

import { withStyles } from "@material-ui/core/styles";
import style, { popover as PopoverStyle } from "./style";

class SearchBar extends React.Component {
    state = { searchContents: "" }
    get adornment() {
        return (
            <InputAdornment position="end">
                <IconButton type="submit" aria-label="Search" onClick={this.search}>
                    <SearchIcon />
                </IconButton>
            </InputAdornment>
        )
    }

    search = (e) => {
        e.preventDefault();
        alert(this.state.searchContents)
    };

    render() {
        return (
            <form onSubmit={e => this.search(e)}>
                <FormControl>
                    <InputLabel htmlFor="nav-search">Search for Posts</InputLabel>
                    <Input id="nav-search" type="text" autoFocus
                        value={this.state.searchContents}
                        onChange={e => this.setState({ searchContents: e.target.value })}
                        endAdornment={this.adornment}
                    />
                </FormControl>
            </form>
        )
    }
}

@withStyles(style)
export default class SearchTile extends React.Component {
    onToggle = open => e => this.setState({ open });
    state = { open: false };
    target = React.createRef();

    renderPopover() {
        let anchorOrigin = { vertical: "center", horizontal: "right" };
        let transformOrigin = { vertical: "center", horizontal: "left" };
        
        return (
            <Popover
                PaperProps={{ style: PopoverStyle }}
                TransitionComponent={Zoom}
                open={this.state.open}
                anchorEl={this.target.current}
                onClose={this.onToggle(false)}
                anchorOrigin={anchorOrigin}
                transformOrigin={transformOrigin}><SearchBar /></Popover>
        )
    }

    render() {
        return (
            <ClickAwayListener onClickAway={this.onToggle(false)}>
                <Tooltip position="right" content="Search" hideTooltipOnClick>
                        <RootRef rootRef={this.target}>
                            <ListItem button onClick={this.onToggle(true)}>
                                <ListItemIcon><SearchIcon /></ListItemIcon>
                                <ListItemText primary="Search" />
                            </ListItem>
                        </RootRef>
                </Tooltip>
                {this.renderPopover()}
            </ClickAwayListener>
        )
    }
}