import React from "react";
import AsyncComponent from "~/components/AsyncComponent";
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from "@material-ui/core/Grid";
import { Themable } from "./components/Themable";
import { withSettingsAccess } from "~/hoc/SettingsInjector";
import { withStyles } from "@material-ui/core/styles";
import { DEFAULT_COLORS } from "memoryhole-common/dist/colors";
import ApiService from "~/services/api/ApiService";
import Post from "./components/Post";

import { GenericError } from "~/views/Errors";
import NotFound from "./components/404";

import style from "./styles";

@withSettingsAccess
@withStyles(style)
export default class PostView extends AsyncComponent {
    constructor(props) {
        super(props);
        let { match: { params: { id: postId } } } = props;

        this.state = {
            postId,
            loaded: false,
            error: null
        };
    }

    onPostReady = () => {
        let { post } = this.state;

        if(post && post.expireDate) {
            let time = post.expireDate;
            if(time.toString().length === 10) time *= 1000;

            let diff = Math.max(time - new Date().getTime(), 1);
            console.log("Reset timer set to " + diff + "ms");
            setTimeout(() => window.location.reload(), diff);
        }
    }

    async componentDidMount() {
        let { postId } = this.state;

        try {
            let req = await ApiService.getPostById(postId);
            if(req.error) throw new Error(req.error);

            if(req.req.status === 404) {
                this.setState({ loaded: true, post: null });
            } else {
                this.setState({ loaded: true, post: req.payload.post }, this.onPostReady);
            }
        } catch(ex) {
            console.log(ex);
            let message = ex;

            while(message.message) {
                message = message.message;
            }

            if(!ex.status) {
                this.setState({
                    loaded: true,
                    error: { message: "Could not reach the Memory Hole", details: "Sorry, it appears our network is having some technical difficulties. Please try again later, or open an issue on GitHub if the problem persists." }
                });
            } else {
                this.setState({
                    loaded: true,
                    error: { message: "An unknown error has occurred", details: message }
                });
            }
        }
    }

    render() {
        let { classes } = this.props;
        let { post, loaded, error } = this.state;

        if(!loaded) {
            return (
                <div className={classes.loadingOverlay}>
                    <CircularProgress className={classes.spinner} size={75} />
                </div>
            )
        }

        if(error) {
            return (
                <Grid container spacing={0}>
                    <Grid item xs={12} className={classes.wrapper}>
                        <Grid item md={8}>
                            <GenericError
                                className={classes.paper}
                                message={error.message}
                                details={error.details} />
                        </Grid>
                    </Grid>
                </Grid>
            )
        }

        if(!post) {
            return (
                <NotFound />
            );
        }

        let theme = {
            ...DEFAULT_COLORS,
            ...(post.colors || {})
        };

        return (
            <Themable theme={theme}>
                <Post post={post} />
            </Themable>
        );
    }
}