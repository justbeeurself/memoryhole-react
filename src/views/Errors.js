import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";

export class GenericError extends Component {
    render() {
        return (
            <Paper className={this.props.className}>
                <h2>{this.props.message}</h2>
                <p>{this.props.details}</p>
                <Link to="/">Home</Link>
            </Paper>
        )
    }
}

export class NotFound extends Component {
    render() {
        return (
            <GenericError
                message="404 Not Found"
                details="Sorry, the resource you are looking for does not exist." />
        );
    }
}