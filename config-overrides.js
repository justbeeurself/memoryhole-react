const webpack = require("webpack");
const { injectBabelPlugin, compose } = require('react-app-rewired');
const rewireEslint = require('react-app-rewire-eslint');
const chalk = require("chalk");
const path = require("path");
const fs = require("fs");

module.exports = {
    webpack: (config, env) => {
		let cfgPath = path.join(__dirname, "config", "config.json");
		let envSpecificCfgPath = path.join(__dirname, "config", `config.${env}.json`);
		if(fs.existsSync(envSpecificCfgPath)) {
			console.log(chalk.green("Found environment-specific config " + env));
			cfgPath = envSpecificCfgPath;
        }
		
        config = Object.assign({}, config, {
            resolve: Object.assign({}, config.resolve, {
                alias: Object.assign({}, config.resolve.alias, {
                    "~": path.resolve(__dirname, "src/"),
                    "config$": cfgPath
                }),
                plugins: [...(config.resolve.plugins || [])].filter(x => x.constructor ? x.constructor.name !== "ModuleScopePlugin" : true)
            })
        });

        config = injectBabelPlugin("transform-decorators-legacy", config);
        config = injectBabelPlugin("transform-object-rest-spread", config);
        config = rewireEslint(config, env);
        
        return config;
    }
}