import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import blueGrey from "@material-ui/core/colors/blueGrey";
import pink from "@material-ui/core/colors/pink";

export const theme = createMuiTheme({
    palette: {
        primary: {
            light: blueGrey[300],
            main: blueGrey[500],
            dark: blueGrey[700]
        },
        secondary: {
            light: pink[300],
            main: pink[500],
            dark: pink[700]
        }
    },
});

export default Component => props => (
    <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...props} />
    </MuiThemeProvider>
);