import React from "react";
import { withSettingsAccess } from "~/hoc/SettingsInjector";
import { withStyles, classNames } from "~/hoc/StyleComposer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Icon from "@material-ui/core/Icon";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Switch from "@material-ui/core/Switch";
import AsyncComponent from "~/components/AsyncComponent";
import PasswordField from "~/components/form/PasswordField";
import ColorPicker from "~/components/ColorPicker";
import Collapse from "@material-ui/core/Collapse";
import settings from "~/services/settings";

import style from "./styles";

@withSettingsAccess
@withStyles(style)
export default class SettingsList extends AsyncComponent {
    state = {
        collapsedGroups: []
    };

    onChangeSetting = (key,type) => e => {
        let { settings } = this.props;

        if(type === "text") {
            settings.setValue(key, e.target.value);
        } else if(type === "boolean") {
            settings.setValue(key, e.target.checked);
        } else if(type === "color") {
            settings.setValue(key, e);
        }
    }

    toggleCollapse = key => e => {
        let collapsedGroups = [...this.state.collapsedGroups];

        if(collapsedGroups.includes(key)) {
            collapsedGroups.splice(collapsedGroups.indexOf(key), 1);
        } else collapsedGroups.push(key);

        this.setState({ collapsedGroups });
    }

    renderSettingGroup = (key, setting) => {
        let { collapsedGroups } = this.state;
        let children = settings.getChildSettings(key);
        
        return (
            <React.Fragment key={key}>
                <ListItem button onClick={this.toggleCollapse(key)}>
                    {setting.icon && (
                        <ListItemIcon><Icon>{setting.icon}</Icon></ListItemIcon>
                    )}
                    <ListItemText primary={setting.label} />
                    <Icon>{collapsedGroups.includes(key) ? "expand_more" : "expand_less"}</Icon>
                </ListItem>
                <Collapse in={collapsedGroups.includes(key)} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        {children.map(child => this.renderSetting(`${key}.${child.id}`, child, true))}
                    </List>
                </Collapse>
            </React.Fragment>
        )
    }

    renderSetting = (key, setting, isChild = false) => {
        let { classes } = this.props;
        if(setting.parent && !isChild) return null;

        if(setting.hasChildren)
            return this.renderSettingGroup(key, setting);

        let addon;
        let onTriggerClick = e => null;

        if(setting.type === "boolean") {
            addon = (
                <Switch
                    onChange={this.onChangeSetting(key, "boolean")}
                    checked={!!setting.value} />
            );
        } else if(setting.type === "password") {
            addon = (
                <PasswordField placeholder="None"
                    defaultValue={(setting.value || "").toString()}
                    onChange={this.onChangeSetting(key, "text")}
                    margin="normal" />
            )
        } else if(setting.type === "color") {
            addon = (
                <ColorPicker
                    icon="palette"
                    color={setting.value}
                    onChange={this.onChangeSetting(key, "color")}
                    ButtonProps={{variant: "contained"}} />
            )
        }

        return (
            <ListItem button key={key} className={classNames(isChild && classes.nested)} onClick={onTriggerClick}>
                {setting.icon && (
                    <ListItemIcon><Icon>{setting.icon}</Icon></ListItemIcon>
                )}
                <ListItemText primary={setting.label} />
                <ListItemSecondaryAction>
                    {addon}
                </ListItemSecondaryAction>
            </ListItem>
        )
    }

    resetSettings = e => {
        e.preventDefault();
        settings.resetToDefaults();
    }

    renderSubheader = classes => (
        <ListSubheader className={classes.header}>
            <span>Settings</span>
            <a href="#/" onClick={this.resetSettings}>Reset</a>
        </ListSubheader>
    )

    render() {
        let { classes } = this.props;
        return (
            <List subheader={this.renderSubheader(classes)}>
                {settings.getSettingKeys().map(key => this.renderSetting(key, settings.get(key)))}
            </List>
        )
    }
}