import React from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Tooltip from "@atlaskit/tooltip";
import { Link } from "react-router-dom";

const style = theme => ({
    selected: {
        background: "#ebebeb",
        borderRight: ".25rem solid " + theme.palette.primary.main
    }
})

@withStyles(style)
export default class Tile extends React.Component {
    render() {
        let { icon: Icon, title, link, redirect, onClick= () => null, selected = false, classes } = this.props;

        let listItemProps = {
            button: true,
            onClick,
            className: classNames(selected && classes.selected)
        };

        if(link) {
            listItemProps = {
                ...listItemProps,
                component: "a",
                href: link,
                target: "_blank"
            }
        } else if(redirect) {
            listItemProps = {
                ...listItemProps,
                component: Link,
                to: redirect
            }
        }

        return (
            <Tooltip position="right" content={title} hideTooltipOnClick>
                <ListItem {...listItemProps}>
                    <ListItemIcon><Icon /></ListItemIcon>
                    <ListItemText primary={title} />
                </ListItem>
            </Tooltip>
        );
    }
}