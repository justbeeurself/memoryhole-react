export default theme => ({
    topCard: {
        display: "flex",
        justifyContent: "center"
    },
    paper: {
        padding: "1rem"
    }
})