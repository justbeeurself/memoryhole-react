const express = require("express");
const path = require("path");
const app = express();

const indexFile = path.join(__dirname, "../build/index.html");
const buildDir = path.resolve(__dirname, "../build");
app.use(express.static(buildDir, {
    index: false
}));

app.get("/*", (req, res) => res.sendFile(indexFile));

app.listen(8080, () => {
    console.log("The Memory Hole is now open...");
});