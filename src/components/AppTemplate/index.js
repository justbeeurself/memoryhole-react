import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import ToggleIcon from "material-ui-toggle-icon";
import classNames from "classnames";
import Drawer from "@material-ui/core/Drawer";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Tiles from "./components/Tiles";
import { isResponsive } from "~/hoc/Responsive";

import styles from "./styles";

@isResponsive
@withStyles(styles, { withTheme: true })
export default class AppTemplate extends Component {
    state = { open: false };

    toggleDrawer = () =>
        this.setState({ open: !this.state.open });

    handleDrawerClose = () =>
        this.setState({ open: false });

    getCloseIcon() {
        let { theme } = this.props;
        return theme.direction === "rtl" ? <ChevronRightIcon /> : <ChevronLeftIcon />
    }

    shouldComponentUpdate = (props, state) =>
        state.open !== this.state.open || props.isMobile !== this.props.isMobile;

    render() {
        let { classes, isMobile } = this.props;

        let drawerClass = {
            paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
        };

        return (
            <div className={classes.root}>
                <AppBar color="primary" position="absolute" className={classes.appBar}>
                    <Toolbar disableGutters>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Close Drawer" onClick={this.toggleDrawer}>
                            <ToggleIcon on={this.state.open} onIcon={this.getCloseIcon()} offIcon={<MenuIcon />} />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <Drawer variant={isMobile ? undefined : "permanent"} classes={drawerClass} open={this.state.open} onClose={this.handleDrawerClose}>
                    <div className={classes.toolbar} />
                    <Tiles />
                </Drawer>
                <div className={classes.content}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}