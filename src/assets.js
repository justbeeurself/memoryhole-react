import jss from "jss";
import nestedJSS from "jss-nested";
import globalJSS from "jss-global";
import composeJSS from "jss-compose"
import "./styles/waves.css";
import "./styles/app.css";

jss.use(
    globalJSS(),
    composeJSS(),
    nestedJSS()
);