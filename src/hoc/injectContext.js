import React from "react";
import nanoid from "nanoid";

export const withContext = (Context,key) => (Component,prop,descriptor) => {
    let id = nanoid(8);
    let uniqueKey = `${key}_ctxWrapper_${id}`;

    if(prop === "render") {
        const render = Component[prop];
        descriptor.value = function() {
            return (
                <Context.Provider key={uniqueKey} value={this.state[key]}>
                    {render.apply(this)}
                </Context.Provider>
            )
        }
    } else {
        return props => {
            return (
                <Context.Consumer key={uniqueKey}>
                    {value => (
                        <Component
                            {...Object.assign({}, props, { [key]: value })} />
                    )}
                </Context.Consumer>
            )
        }
    }
}