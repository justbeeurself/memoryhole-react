import "babel-polyfill";
import React from "react";
import Enzyme, { render, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16"
import { stub } from "sinon";
import { expect } from "chai";
import { createStore } from "redux";
import {StaticRouter, Route} from "react-router-dom";
import { Provider } from "react-redux";
import { JSDOM } from "jsdom";

import axios from "axios";
import moxios from "axios-mock-adapter";

const {document: doc} = new JSDOM("<!doctype html><html><body></body></html>").window;
global.document = doc;
global.window = doc.defaultView;
global.NodeList = window.NodeList;
global.HTMLElement = window.HTMLElement;
global.Element = window.Element;
global.Document = window.Document;

global.localStorage = mockLocalStorage();

global.navigator = {
    userAgent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0"
}

Enzyme.configure({
    adapter: new Adapter()
});

export function mockLocalStorage() {
    var storage = {};

    return {
        setItem: function (key, value) {
            storage[key] = value || "";
        },
        getItem: function (key) {
            return key in storage ? storage[key] : null;
        },
        removeItem: function (key) {
            delete storage[key];
        },
        get length() {
            return Object.keys(storage).length;
        },
        key: function (i) {
            var keys = Object.keys(storage);
            return keys[i] || null;
        }
    };
}

export function mockAPI() {
    let funcMap = {
        "get": "onGet",
        "post": "onPost",
        "delete": "onDelete"
    };

    let setup = () => {
        let mock = new moxios(axios, {
            delayResponse: 200
        });

        let fakeEndpoint = (endpoint, method, cb) => {
            mock[funcMap[method]](endpoint).reply((cfg) => cb(cfg));
        }

        let cleanup = () => mock.restore();

        return { mock, fakeEndpoint, cleanup };
    };

    return {
        funcMap, setup
    };
}

export function renderWithFormAndLink(Component) {
    let store = createStore(() => ({}));
    return mount(
        <Provider store={store}>
            <StaticRouter context={{}}>
                <Route render={() => Component} />
            </StaticRouter>
        </Provider>
    );
}

export function testForm(FormComponent, fields) {
    let mock = {
        handleSubmit: function(handler) {
            return (e) => {
                // e.preventDefault();
                // let target = $(e.target);
                // let values = target.serialize();
                // handler(values, e);
                handler({}, e);
            }
        },
        onSubmit: stub().withArgs("values")
    }
    
    let component = renderWithFormAndLink(<FormComponent handleSubmit={mock.handleSubmit} onFormSubmit={mock.onSubmit} />);

    it("renders", () => {
        expect(component).to.have.lengthOf(1);
    });

    it("submits", () => {
        component.simulate("submit");
        
        // //No bouncing
        expect(mock.onSubmit.callCount).to.equal(1);

        //All fields included
        let values = mock.onSubmit.args[0][0];
        fields.forEach(field => {
            expect(values).to.have.property(field);
            expect(values[field]).to.equal("");
        });
    });
}