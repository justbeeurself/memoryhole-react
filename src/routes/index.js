import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import TemplatedRoute from "~/components/TemplatedRoute";
import { asyncLoad } from "~/components/AsyncComponent";

const NotFound = asyncLoad(() => import("~/views/Errors"), "NotFound");
const Index = asyncLoad(() => import("~/views/Index"));
const Settings = asyncLoad(() => import("~/views/Settings"));
const Post = asyncLoad(() => import("~/views/Post"));
const Search = asyncLoad(() => import("~/views/Search"));

export default class Routes extends Component {
    searchByUser = props => (
        <Search {...props} searchMode="identity" />
    )

    searchGeneric = props => (
        <Search {...props} searchMode="generic" />
    )

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <TemplatedRoute exact path="/" component={Index} />
                    <TemplatedRoute exact path="/settings" component={Settings} />
                    <TemplatedRoute exact path="/search" render={this.searchGeneric} />
                    <TemplatedRoute exact path="/u/:identity" render={this.searchByUser} />
                    <Route exact path="/p/:id" component={Post} />
                    <Route component={NotFound} />
                </Switch>
            </BrowserRouter>
        );
    }
};