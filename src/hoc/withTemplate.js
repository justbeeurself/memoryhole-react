import React from "react";
import AppTemplate from "~/components/AppTemplate";

export default Component => props => (
    <AppTemplate>
        <Component {...props} />
    </AppTemplate>
);