import React, { Component } from "react";
import EasyMDE from "easymde/src/js/easymde";
import "easymde/dist/easymde.min.css";
import setupToolbar from "./toolbar";
import classNames from "classnames";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { Picker as EmojiPicker } from "emoji-mart";
import { Manager, Popper } from "react-popper";
import { withStyles } from "~/hoc/StyleComposer";
import styles from "./styles";
import markdownStyles from "~/styles/markdown";

import settings from "~/services/settings";
import twemoji from "~/deps/twemoji";
import * as Emoji from "~/lib/emojis";

const parseEmojis = html => {
    let out = html;

    if(settings.getValue("parseEmojis", true)) {
        out = Emoji.parse(html);
    };

    return twemoji.parse(out);
}

let markdownStyle = {
    markdown: {
        "& .editor-preview-side.editor-preview-active-side": markdownStyles.markdown
    }
};

@withStyles(styles, markdownStyle)
export default class MarkdownEditor extends Component {
    state = {
        fullscreen: false,
        selectingEmoji: false
    }

    componentDidMount() {
        this.mde = new EasyMDE({
            onToggleFullScreen: fullscreen => this.setState({ fullscreen }),
            beforeUpdatePreview: parseEmojis,
            autoDownloadFontAwesome: false,
            element: this.textarea,
            indentWithTabs: true,
            placeholder: "",
            spellChecker: false,
            tabSize: 4,
            toolbar: setupToolbar.call(this)
        });

        this.mde.codemirror.on("blur", () => this.props.onBlur({ target: this.textarea, persist: () => null }));

        this.mde.codemirror.on("change", () => {
            this.textarea.value = this.mde.value();
            this.onChange();
        });
    }

    onChange() {
        if(this.props.onChange) {
            this.props.onChange({ target: this.textarea, persist: () => null });
        }
    }

    showEmojiPicker = () => {
        if (/editor-preview-active/.test(this.mde.codemirror.getWrapperElement().lastChild.className))
            return;

        this.setState(state => ({ selectingEmoji: !state.selectingEmoji }));
    }

    injectEmoji = (emoji) => {
        let { codemirror } = this.mde;

        if (/editor-preview-active/.test(codemirror.getWrapperElement().lastChild.className))
            return;

        var startPoint = codemirror.getCursor('start');
        let text = codemirror.getLine(startPoint.line);
        let start = text.slice(0, startPoint.ch);
        let end = text.slice(startPoint.ch);

        codemirror.replaceRange(start + emoji.colons + end, {
            line: startPoint.line,
            ch: 0
        }, {
            line: startPoint.line,
            ch: Number.MAX_VALUE
        });

        codemirror.setSelection(startPoint);
    }

    componentWillUnmount() {
        this.mde.toTextArea();
        this.mde = null;
    }

    get emojiPicker() {
        let { classes } = this.props;
        let { selectingEmoji } = this.state;
        if(!selectingEmoji) return null;
        
        return (
            <ClickAwayListener onClickAway={e => this.setState({ selectingEmoji: false })}>
                <Manager>
                    <Popper referenceElement={this.mde.toolbarElements["emojis"]} placement="bottom">
                        {({ ref, style, placement, arrowProps }) => (
                            <div ref={ref} style={style} data-placement={placement} className={classes.popover}>
                                <EmojiPicker emojiTooltip set="twitter" title="" emoji="" onSelect={this.injectEmoji} />
                                <div ref={arrowProps.ref} style={arrowProps.style} />
                            </div>
                        )}
                    </Popper>
                </Manager>
            </ClickAwayListener>
        )
    }

    componentDidUpdate() {
        if(this.state.selectingEmoji && !/active/.test(this.mde.toolbarElements["emojis"].className)) {
            this.mde.toolbarElements["emojis"].className += " active"
        } else {
            this.mde.toolbarElements["emojis"].className = this.mde.toolbarElements["emojis"].className.replace(/\s*active\s*/g, '');
        }
    }

    render() {
        let { error, classes, ...otherProps } = this.props;

        return (
            <div className={classNames(classes.editor, classes.markdown, this.state.fullscreen && "fullscreen-editor-mode", error && "is-errored")}>
                <textarea ref={ref => this.textarea = ref} {...otherProps} />
                {this.emojiPicker}
            </div>
        );
    }
}