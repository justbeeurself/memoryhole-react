import color from "color";
import markdownStyle from "~/styles/markdown";
import merge from "lodash/merge"

export default theme => ({
    view: {
        backgroundColor: theme.background,
        height: "100%",
        overflow: "auto"
    },

    wrapper: {
        marginTop: "1rem",
        display: "flex",
        justifyContent: "center",
        color: theme.text,

        "&:last-of-type": {
            marginBottom: "1rem",
        },

        "@media (max-width: 959px)": {
            marginTop: 0,

            "&:last-of-type": {
                height: "100%"
            }
        }
    },

    paper: {
        padding: "1rem",
        backgroundColor: theme.foreground,
        color: theme.text,

        "@media (max-width: 959px)": {
            borderRadius: 0,
            boxShadow: "none"
        }
    },

    breadcrumb: {
        marginBottom: "1rem",
        "& > *": {
            color: theme.link,
            display: "inline-block",
            marginRight: ".5rem"
        },
    },

    breadcrumbSeparator: {
        color: theme.text
    },

    themedText: {
        color: theme.text,

        "a&": {
            color: theme.link,
            cursor: "pointer",
            textDecoration: "none"
        }
    },

    markdown: merge({}, markdownStyle.markdown, {
        color: theme.text,

        "& h1, & h2, & h3, & h4, & h5, & h6": {
            color: theme.text
        },

        "& strong": {
            color: theme.text
        },

        "& a": {
            color: theme.link
        },

        "& pre, & pre > code": {
            backgroundColor: theme.code,
            color: theme.quote
        },

        "&& code": {
            backgroundColor: theme.code,
            color: theme.accent
        }
    })
});