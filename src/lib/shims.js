import React from "react";
import nanoid from "nanoid";

const changeHandlers = {};

const setItem = Storage.prototype.setItem;
Storage.prototype.setItem = function(...args) {
    Object.getOwnPropertyNames(changeHandlers).forEach(x => changeHandlers[x].apply(this, args));
    setItem.apply(this, args);
}

export class LocalStorageListener extends React.Component {
    id = nanoid(12);

    componentDidMount() {
        changeHandlers[this.id] = this.props.onChange;
    }

    componentDidUpdate(props) {
        changeHandlers[this.id] = this.props.onChange;
    }

    componentWillUnmount() {
        delete changeHandlers[this.id];
    }

    render() {
        return this.props.children || null;
    }
}