import React from "react";
import { withStyles } from "@material-ui/core/styles";

const style = theme => ({
    container: {
        marginTop: "1rem",
        marginBottom: "1rem",

        "@media (max-width: 959px)": {
            "& > div": {
                marginLeft: "auto",
                marginRight: "auto"
            }
        }
    }
});

@withStyles(style)
export default class Recaptcha extends React.Component {
    shouldComponentUpdate() {
        return false;
    }

    onLoadRecaptcha() {
        let { grecaptcha } = window;
        let {
            sitekey,
            theme = "light",
            size = "normal",
            tabindex = 0
        } = this.props;

        grecaptcha.render(this.container, {
            sitekey, theme, size, tabindex,
            callback: "grecaptchaSuccessfulValidationCallbackFunc",
            "expired-callback": "grecaptchaExpiredCallbackFunc"
        });
    }

    componentWillMount() {
        let script = document.createElement("script");
        script.src = "https://www.google.com/recaptcha/api.js?onload=grecaptchaCallbackFunc&render=explicit";
        script.async = true;

        window.grecaptchaCallbackFunc = () => this.onLoadRecaptcha();
        window.grecaptchaSuccessfulValidationCallbackFunc = (...args) => this.props.onVerify(...args);
        window.grecaptchaExpiredCallbackFunc = (...args) => this.props.onExpire(...args);

        document.head.appendChild(script);
        this.script = script;
    }

    componentWillUnmount() {
        if(document.head.contains(this.script)) {
            document.head.removeChild(this.script);
        }
        this.script = null;

        //Google likes to make a big mess of the DOM...
        [...document.querySelectorAll("html > script")].forEach(elem => elem.parentNode.removeChild(elem));
        [...document.querySelectorAll("body > div:not(#root)")].forEach(elem => elem.parentNode.contains(elem) && elem.parentNode.removeChild(elem));

        delete window.grecaptchaCallbackFunc;
        delete window.grecaptchaSuccessfulValidationCallbackFunc;
        delete window.grecaptchaExpiredCallbackFunc;
    }

    render() {
        let { classes } = this.props;

        return (
            <div className={classes.container} ref={ref => this.container = ref} />
        );
    }
}