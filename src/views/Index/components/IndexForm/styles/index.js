export default theme => ({
    "@keyframes loop": {
        "0%": { transform: "rotateZ(0deg)" },
        "50%": { transform: "rotateZ(180deg)" },
        "100%": { transform: "rotateZ(359deg)" }
    },

    expandedPanel: {
        overflow: "visible"
    },

    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    margin: {
        margin: theme.spacing.unit
    },
    titleError: {
        marginBottom: "1rem"
    },
    titleInput: {
        fontSize: "2rem"
    },
    titleWrapper: {
        paddingLeft: ".5rem",
        paddingRight: ".5rem"
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    loop: {
        animation: "loop infinite 1s"
    },
    submitButton: {
        padding: "1rem 3rem",

        "@media (max-width: 959px)": {
            width: "100%",
            padding: "2rem 3rem"
        }
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    content: {
        display: "flex",
        flexDirection: "column"
    },
    field: {
        display: "block",
        width: "100%"
    }
});