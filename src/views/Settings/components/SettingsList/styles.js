export default theme => ({
    header: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between"
    },

    nested: {
        padding: ".75rem 32px .75rem 4rem"
    }
});