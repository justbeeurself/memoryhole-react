import { parseDictionary, variationsData } from "~/lib/emojiDictionary";

let dict = {};

for(let i in parseDictionary) {
    for(let j=0; j < parseDictionary[i][3].length; j++){
        dict[parseDictionary[i][3][j]] = i;
    }
}

const findImage = (idx, var_idx) => {
    let out = {
        path: "",
        sheet: "",
        sheet_size: 0,
        px: parseDictionary[idx][4],
        py: parseDictionary[idx][5],
        full_idx: idx,
        is_var: false,
        unified: parseDictionary[idx][0][0]
    }

    // can we use a variation?
    if (var_idx && variationsData[idx] && variationsData[idx][var_idx]){
        let var_data = variationsData[idx][var_idx];

        out.px = var_data[1];
        out.py = var_data[2];
        out.full_idx = var_data[0];
        out.is_let = true;
        out.unified = var_data[4];
    }

    return out;
}

export const replaceText = (idx, wrapper = "", variation) => {
    let extra = "";
    let var_idx = null;
    if (typeof variation === "object") {
        extra = replaceText(variation.idx, variation.wrapper, undefined);
        var_idx = variation.idx;
    }

    let img = findImage(idx, var_idx);
    
    if (img.is_var)
        extra = "";

    return img.unified + extra;
};

export const parseColons = text => {
    let regex = /:[a-zA-Z0-9-_+]+:(:skin-tone-[2-6]:)?/g;

    return text.replace(regex, (match) => {
        let idx = match.substr(1, match.length-2).toLowerCase();

        // special case - an emoji with a skintone modified
        if (idx.indexOf("::skin-tone-") > -1){
            let skin_tone = idx.substr(-1, 1);
            let skin_idx = "skin-tone-"+skin_tone;
            let skin_val = dict[skin_idx];
            idx = idx.substr(0, idx.length - 13);

            let val = dict[idx];

            if (val){
                return replaceText(val, ":", {
                    "idx"		: skin_val,
                    "actual"	: skin_idx,
                    "wrapper"	: ":"
                });
            }else{
                return ":" + idx + ":" + replaceText(skin_val, ":");
            }
        } else {
            let val = dict[idx];
            return val ? replaceText(val, ":") : match;
        }
    });
}

export const parse = text => parseColons(text);