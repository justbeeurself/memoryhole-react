import blueGrey from '@material-ui/core/colors/blueGrey';

export default theme => ({
    loadingOverlay: {
        position: "fixed",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },

    wrapper: {
        marginTop: "1rem",
        display: "flex",
        justifyContent: "center",

        "&:last-of-type": {
            marginBottom: "1rem"
        }
    },

    paper: {
        padding: "1rem"
    },

    spinner: {
        margin: theme.spacing.unit * 2,
        color: blueGrey[500]
    }
});