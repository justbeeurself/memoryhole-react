import injectSheet, { createTheming } from "react-jss";

const theming = createTheming("memoryHolePost");
export const { ThemeProvider: Themable } = theming;

export const withPostTheme = styles => Component => injectSheet(styles, { theming })(Component)