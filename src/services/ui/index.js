import Waves from "~/lib/waves";

export const setPageTitle = title => {
    let out = "The Memory Hole";

    if(title && title.length)
        out = `${title} - ${out}`;

    document.title = out;
}

export const onPageLoad = func => {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        func();
    } else {
        document.addEventListener("DOMContentLoaded", func);
    }
}

onPageLoad(() => {
    Waves.init();
});