require("babel-register")({
    presets: [ "env", "react" ],
    plugins: [
        "transform-decorators-legacy",
        "transform-class-properties",
        "transform-object-rest-spread",
        ["module-resolver", {
            "root": "../src",
            alias: {
                "~": "./src/"
            }
        }]
    ]
});