import except from "lodash/omit";
import pickBy from "lodash/pickBy";

const parseDefaults = settings => Object.getOwnPropertyNames(settings).reduce((allSettings, key) => {
    if(!!!settings[key]) return allSettings;
    
    if(settings[key].hasChildren)
        return { ...allSettings, [key]: null };

    return {
        ...allSettings,
        [key]: settings[key].default
    }
}, {});

const parseOptions = settings => Object.getOwnPropertyNames(settings).reduce((allSettings, key) => {
    let setting = settings[key];

    if(!!setting && setting.type === "group") {
        let optionSet = {};

        optionSet[key] = {
            ...except(setting, "items"),
            hasChildren: true
        };

        setting.items.forEach(item => {
            optionSet[`${key}.${item.id}`] = {...item};
            optionSet[`${key}.${item.id}`].parent = key;
        });
        
        return {
            ...allSettings,
            ...optionSet
        }
    } else {
        return {
            ...allSettings,
            [key]: settings[key]
        }
    }
}, {});

export default new class Settings {
    initialLoad = false;
    rawOpts = {};
    options = {};
    cachedSettings = {};
    defaultSettings = {};

    loadSettings = (options = this.rawOpts, init = false) => {
        this.rawOpts = options;
        this.options = parseOptions(options);
        let defaults = parseDefaults(this.options);
        this.defaultSettings = defaults;

        let settings = localStorage.getItem("settings");

        try {
            if(settings === null) throw new Error("Settings not yet saved");

            settings = JSON.parse(settings);
            this.cachedSettings = {
                ...this.defaultSettings,
                ...this.cachedSettings,
                ...settings
            };
        } catch(ex) {
            console.log("Could not load settings from localStorage, overriding with defaults");
            localStorage.setItem("settings", JSON.stringify(defaults))
            this.cachedSettings = defaults;
        }

        this.initialLoad = true;
    }

    resetToDefaults() {
        let options = this.rawOpts;
        
        this.options = parseOptions(options);
        this.defaultSettings = parseDefaults(this.options);
        this.cachedSettings = this.defaultSettings;
        this.flushSettings();
    }

    get = (key, defaultValue = null) => {
        let setting = this.options[key];
        if(setting) {
            return {
                ...setting,
                valid: true,
                value: this.getValue(key, defaultValue)
            };
        } else return { valid: false, value: defaultValue };
    }

    getChildSettings = (key) => {
        let setting = this.options[key];

        if(setting) {
            return Object.values(this.options).filter(x => x.parent === key).map(x => this.get(`${key}.${x.id}`));
        } else return [];
    }

    getValue = (key, defaultValue = null) => {
        let setting = undefined;

        setting = this.cachedSettings[key];
        let opt = this.rawOpts[key];

        if(!(key in this.cachedSettings)) {
            if(!!opt && !!!opt.hasChildren) {
                this.setValue(key, defaultValue);
            }
            return defaultValue;
        }

        return setting;
    }

    setValue = (key, value) => {
        this.cachedSettings[key] = value;
        this.flushSettings();
    }

    getSettingKeys = () => Object.getOwnPropertyNames(this.options);

    flushSettings = () =>
        localStorage.setItem("settings", JSON.stringify(this.cachedSettings));
}()