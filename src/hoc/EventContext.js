import React from "react";

/**
 * EventContext is a way for any subscribed component to execute a function defined by the EventContext
 * Only render one EventContext per createEventContext
 * @param {Name of the event (used to inject into props of subscribed components)} eventName 
 */
export const createEventContext = eventName => {
    let handler = () => undefined;

    const subscribe = Component => props => {
        return React.createElement(Component, {
            ...props,
            events: Object.assign({}, props.events, {
                [eventName]: (...args) => handler(...args)
            })
        });
    }

    class EventContext extends React.Component {
        componentWillMount() {
            handler = this.props.handler;
        }

        componentWillUnmount() {
            handler = () => undefined;
        }

        render() {
            return this.props.children;
        }
    }

    return { EventContext, subscribe };
}