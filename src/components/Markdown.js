import React, { Component } from "react";
import parseMarkdown from "marked";
import { default as SanitizeState } from "marked-sanitizer-github";
import settings from "~/services/settings";
import twemoji from "~/deps/twemoji";
import * as Emoji from "~/lib/emojis";

const parseEmojis = html => {
    let out = html;

    if(settings.getValue("parseEmojis", true)) {
        out = Emoji.parse(html);
    };

    return twemoji.parse(out);
}

const renderer = new parseMarkdown.Renderer();
const linkRenderer = renderer.link;
renderer.link = (href, title, text) => {
    const html = linkRenderer.call(renderer, href, title, text);

    if(href[0] !== "#") { //Hash links shouldn't open in new tabs...
        return html.replace(/^<a /, '<a target="_blank" rel="nofollow" ');
    }

    return html;
};

const imageRenderer = renderer.image;
renderer.image = (href, title, text) => {
    if(settings.getValue("autoRenderImages", true)) {
        return imageRenderer.call(renderer, href, title, text);
    }

    return linkRenderer.call(renderer, href, title, text);
}

export default class Markdown extends Component {
    state = {
        originalContent: null,
        content: null
    }

    getSanitizedInput() {
        let { content } = this.props;

        return new Promise((resolve, reject) => {
            let props = { sanitize: true, sanitizer: new SanitizeState().getSanitizer(), gfm: true, renderer, breaks: true };

            parseMarkdown(content, props, (err, content) => {
                if(err) return reject(err);
                
                let parsed = parseEmojis(content);
                return resolve(parsed);
            });
        });
    }
      

    async componentWillMount() {
        try {
            let content = await this.getSanitizedInput();
            this.setState({ content });
        } catch(ex) {
            console.log(ex);
            alert("An error occurred while parsing some Markdown, please try again later or submit an issue to the developers.");
            this.props.history.push("/");
        }
    }

    shouldComponentUpdate(props) {
        return props.content !== this.state.originalContent;
    }

    async componentDidUpdate() {
        if(this.state.originalContent !== this.props.content) {
            try {
                let content = await this.getSanitizedInput();
    
                if(content !== this.state.content) {
                    this.setState({ originalContent: this.props.content, content });
                }
            } catch(ex) {
                console.log(ex);
                alert("An error occurred while parsing some Markdown, please try again later or submit an issue to the developers.");
                this.props.history.push("/");
            }
        }
    }
    
    render() {
        let { className } = this.props;
        
        return (
            <div className={["content", className].join(" ")} dangerouslySetInnerHTML={{__html: this.state.content}} />
        );
    }
}