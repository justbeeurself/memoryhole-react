import React from "react";
import { withStyles } from "~/hoc/StyleComposer";
import AsyncComponent from "~/components/AsyncComponent";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "@material-ui/core/Button";
import * as colorset from "@material-ui/core/colors";
import colorUtil from "color";
import ColorfulSlider from "../ColorfulSlider";
import style from "./style";

let accents = [ 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, "A100", "A200", "A400", "A700" ];
let accentNames = accents.slice(0, 10).concat(["Accent 1", "Accent 2", "Accent 3", "Accent 4"]);

let colors = [
    "red", "pink", "purple",
    "deepPurple", "indigo", "blue",
    "lightBlue", "cyan", "teal",
    "green", "lightGreen", "lime",
    "yellow", "amber", "orange",
    "deepOrange", "brown", "grey",
    "blueGrey"
];

let colorNames = [
    "Red", "Pink", "Purple", "Deep Purple", "Indigo", "Blue", "Light Blue", "Cyan", "Teal",
    "Green", "Light Green", "Lime", "Yellow", "Amber", "Orange", "Deep Orange", "Brown", "Greyscale", "Blue Grey"
]

const getColorByHex = hex => {
    for(let group = 0; group < colors.length; group++) {
        let set = colorset[colors[group]];
        let items = Object.entries(set);

        for(let color = 0; color < items.length; color++) {
            if(items[color][1].toLowerCase() === hex) {
                return { group, color };
            }
        }
    }

    return { group: 0, color: 5 }
}

let totalColors = colors.length - 1;

@withStyles(style)
export default class ColorPickerModal extends AsyncComponent {
    state = getColorByHex(this.props.selectedColor || "#039be5");

    componentDidUpdate({ selectedColor, open }) {
        if(selectedColor !== this.props.selectedColor || open !== this.props.open) {
            this.setState(getColorByHex(this.props.selectedColor));
        }
    }

    get chosenColor() {
        let { group, color } = this.state;
        return colorset[colors[group]][accents[color]];
    }

    get colorGroup() {
        return colorset[colors[this.state.group]];
    }

    getMaxColorRange(group) {
        return Math.min(Object.keys(colorset[colors[group]]).length - 1, this.state.color)
    }

    onSetCategory = (_, group) => this.setState({ group, color: this.getMaxColorRange(group) });
    onSetColor = (_, color) => this.setState({ color });
    onClose = e => this.props.onClose(this.chosenColor);
    onCancel = e => this.props.onClose();

    render() {
        let { open, classes } = this.props;
        let { group, color } = this.state;
        
        let buttonStyle = {
            backgroundColor: this.chosenColor,
            color: colorUtil(this.chosenColor).isLight() ? "#000" : "#fff"
        }

        return (
            <Dialog open={open} PaperProps={{className: classes.modal}} scroll="paper">
                <DialogTitle>Select a color</DialogTitle>
                <DialogContent>
                    <DialogContentText>Color - {colorNames[group]}</DialogContentText>
                    <ColorfulSlider value={group} min={0} max={totalColors} step={1} onChange={this.onSetCategory} />
                    <DialogContentText>Shade - {accentNames[color]}</DialogContentText>
                    <ColorfulSlider isSubcolor set={group} value={color} min={0} max={Object.keys(this.colorGroup).length - 1} step={1} onChange={this.onSetColor} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.onClose} style={buttonStyle} variant="extendedFab">
                        Choose {this.chosenColor}
                    </Button>
                    <Button color="secondary" onClick={this.onCancel} variant="flat">Cancel</Button>
                </DialogActions>
            </Dialog> 
        )
    }
}