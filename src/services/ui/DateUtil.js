import take from "lodash/take";
import moment from "moment";

const pluralize = (word, count) => 
    count === 1 ? word : word + "s";

export const humanizeDuration = duration => {
    const durationParts = [
        { value: duration.years(), unit: "year" },
        { value: duration.months(), unit: "month" },
        { value: duration.days(), unit: "day" },
        { value: duration.hours(), unit: "hour" },
        { value: duration.minutes(), unit: "minute" },
        { value: duration.seconds(), unit: "second" }
    ].filter((part) => part.value);

    // Display up to two of the most significant remaining parts
    return take(durationParts, 2).map((part) => (
        `${part.value} ${pluralize(part.unit, part.value)}`
    )).join(", ");
}

export const humanizeDate = seconds => {
    if(seconds.toString().length === 10) {
        seconds *= 1000;
    }

    let diff = new Date().getTime() - seconds;

    if(diff < 1000) {
        return "less than a second ago";
    }

    return humanizeDuration(moment.duration(diff, "milliseconds")) + " ago";
}