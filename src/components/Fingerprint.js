import React from "react";
import createRandomart from "randomart";

const colors = {
    "-2": "E", // end
    "-1": "S", // start
    "0": "#fff",
    "1": ".",
    "2": "o",
    "3": "+",
    "4": "=",
    "5": "*",
    "6": "B",
    "7": "O",
    "8": "X",
    "9": "@",
    "10": "%",
    "11": "&",
    "12": "#",
    "13": "/",
    "14": "^"
}

export default class Fingerprint extends React.Component {

}