import React from "react";
import registerServiceWorker from "~/registerServiceWorker";
import { withSnackbar } from "material-ui-snackbar-provider";

@withSnackbar()
export default class ServiceWorker extends React.Component {
    componentDidMount() {
        let { snackbar } = this.props;
        
        registerServiceWorker(() => {
            snackbar.showMessage("An update is available, please refresh the page.", "Refresh", () => window.location.reload());
        });
    }

    render = () => null;
}