import { expect } from "chai";
import { mockLocalStorage } from "../setup";

describe("Settings", () => {
    before(() => {
        global.localStorage = mockLocalStorage();
    });

    after(() => {
        delete global.localStorage;
    });

    it("Sets defaults", () => {
        const { default: defaults } = require("~/settings");

        let settings = localStorage.getItem("settings");
        expect(settings).to.not.equal(null);

        settings = JSON.parse(settings);
        expect(settings.showHelp).to.equal(defaults.showHelp.default);
    });
})