import purple from "@material-ui/core/colors/purple";
let selectors = {};

export const numberOfLayers = 12;

for(var i = 0; i < numberOfLayers; i++) {
    selectors[`&:nth-child(${i + 1})`] = {
        animationDelay: `${.3 * (i + 1)}s`
    };
}

export default theme => ({
    "@global": {
        "body,html": {
            backgroundColor: "#000 !important"
        }
    },

    error: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        zIndex: 10,
        display: "flex",
        flexDirection: "column",
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
        
        "& *": {
            color: "#fff"
        }
    },

    link: {
        "& a": {
            textDecoration: "none",
            color: purple[400]
        }
    },

    wrapper: {
        position: "absolute",
        top: "50%",
        left: "50%",
        zIndex: 2
    },

    wave: {
        display: "block",
        position: "absolute",
        width: "140px",
        height: "140px",
        left: "-70px",
        top: "-70px",
        borderRadius: "140px",
        opacity: "0",
        animationName: "scale",
        animationDuration: `${numberOfLayers * .3}s`,
        animationIterationCount: "infinite",
        animationTimingFunction: "linear",

        ...selectors
    }
});