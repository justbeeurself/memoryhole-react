import React from "react";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import Tile from "./tiles/Tile";

import HomeIcon from "@material-ui/icons/Home";
import SearchIcon from "@material-ui/icons/Search";

import ReportIcon from "@material-ui/icons/Report";
import CodeIcon from "@material-ui/icons/Code";
import SettingsIcon from "@material-ui/icons/Settings";
import TextFormatIcon from "@material-ui/icons/TextFormat";
import HelpIcon from "@material-ui/icons/Help";

export default (props) => {
    let { pathname } = window.location;
    return (
        <React.Fragment>
            <List>
                <Tile redirect="/" icon={HomeIcon} title="Home" selected={pathname === "/"} />
                <Tile redirect="/search" icon={SearchIcon} title="Search" selected={pathname === "/search"} />
            </List>
            <Divider />
            <List>
                <Tile redirect="/settings" icon={SettingsIcon} title="Settings" selected={pathname === "/settings"} />
                <Tile redirect="/p/markdown-guide" icon={TextFormatIcon} title="Markdown Guide" selected={pathname === "/p/markdown-guide"} />
                <Tile icon={CodeIcon} title="Source Code" link="https://bitbucket.org/justbeeurself" />
                <Tile redirect="/p/about" icon={HelpIcon} title="About" selected={pathname === "/p/about"} />
                <Tile redirect="/report" icon={ReportIcon} title="Report a Problem" selected={pathname === "/report"} />
            </List>
        </React.Fragment>
    )
};