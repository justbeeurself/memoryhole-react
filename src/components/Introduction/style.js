export default theme => ({
    container: {

    },
    mobileContainer: {
        borderRadius: 0
    },
    carousel: {
        position: "absolute"
    },
    icon: {
        fontSize: "10rem",
        color: "#fff"
    }
});