import React from "react";
import { store } from "./store";
import { Provider } from "react-redux";
import Routes from "./routes";

export class ErrorBoundary extends React.Component {
    componentDidCatch(err) {
        console.log(err);
    }

    render() {
        return this.props.children;
    }
}

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Routes />
            </Provider>
        );
    }
}