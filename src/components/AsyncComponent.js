import React, { Component } from "react";

export default class AsyncComponent extends Component {
    onError = (ex) => {
        console.log(ex);
        
    }

    updateState(state) {
        return new Promise((resolve, reject) => {
            try {
                this.setState(state, () => resolve());
            } catch(ex) {
                return reject(ex);
            }
        })
    }
}

export const asyncLoad = (importComponent, target = "default") => (
    class AsyncComponent extends Component {
        state = {
            Component: null
        };

        async componentDidMount() {
            const { [target]: Component } = await importComponent();
            this.setState({ Component });
        }

        render() {
            let { Component } = this.state;
            return Component ? <Component {...this.props} /> : null;
        }
    }
)