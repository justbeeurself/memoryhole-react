import axios from "axios";
import config from "config";

export const BASE_URL = config.apiUrl;

export const createRequest = (headers = {}, watcher = null, overrideTimeout = false) => {
    let opts = {
        baseURL: BASE_URL,
        timeout: overrideTimeout ? 0 :30000,
        validateStatus: false,
        headers: {
            ...headers,
            "Content-Type": "application/json"
        }
    }
    
    if(watcher) opts.onUploadProgress = watcher;
    return axios.create(opts);
}

export const handleError = (ex) => {
    if(ex.code && ex.code === "ECONNABORTED") return { payload: null, error: "Could not contact the server, please contact an administrator" };
    return {payload: null, error: ex};
}

export const handleResponse = (res) => {
    if(res.status === 401) return { payload: null, error: res.data.error, status: res.status };
    if(!([400, 304, 200, 500].includes(res.status))) return { payload: null, error: res.message, status: res.status };
    
    let data = res.data;
    
    if(data.error) return { payload: null, error: data.error, status: res.status };

    return {
        payload: data.data,
        error: null,
        status: res.status
    };
}

export function get({endpoint, watcher = null, data, headers = {}}) {
    return Promise.resolve()
        .then(() => createRequest(headers, watcher))
        .then((req) => req.get(endpoint, data))
        .then(handleResponse)
        .catch(ex => handleError(ex));
}

export function post({endpoint, watcher = null, data, headers = {}, overrideTimeout = false}) {
    return Promise.resolve()
        .then(() => createRequest(headers, watcher, overrideTimeout))
        .then((req) => req.post(endpoint, data))
        .then(handleResponse)
        .catch(ex => handleError(ex));
}

export function put({endpoint, watcher = null, data, headers = {}}) {
    return Promise.resolve()
        .then(() => createRequest(headers, watcher))
        .then((req) => req.put(endpoint, data))
        .then(handleResponse)
        .catch(ex => handleError(ex));
}

//Unfortunately, delete is a reserved token in JS
export function remove({endpoint, watcher = null, data, headers = {}}) {
    return Promise.resolve()
        .then(() => createRequest(headers, watcher))
        .then((req) => req.delete(endpoint, data))
        .then(handleResponse)
        .catch(ex => handleError(ex));
}

//Just a generic way to get/post some data. Used in the list views
export const makeRequest = async (method, url, props, watcher = null) => {
    if(method === "get") {
        let { payload, error } = await get({ endpoint: url, watcher, data: props || {} });
        if(error) throw error;

        return payload;
    } else {
        let { payload, error } = await post({ endpoint: url, watcher, data: props || {} });
        if(error) throw error;

        return payload;
    }
}