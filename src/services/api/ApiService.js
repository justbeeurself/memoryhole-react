import { API } from './index';
import { get, post } from '../net';

class ApiService {
    async getPostById(id) {
        let request = get({
            endpoint: `p/${encodeURIComponent(id)}`
        });

        try {
            let { error, payload } = await request;
            return API.handleResponse(request, payload, error);
        } catch(ex) {
            return API.handleResponse(request, null, ex);
        }
    }

    async createPost(values) {
        let request = post({
            endpoint: `createPost`,
            data: values
        });

        try {
            let { error, payload } = await request;
            return API.handleResponse(request, payload, error);
        } catch(ex) {
            return API.handleResponse(request, null, ex);
        }
    }
}

const inst = new ApiService();
export default inst;