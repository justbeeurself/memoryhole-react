import { DEFAULT_COLORS as theme } from "memoryhole-common/dist/colors";

export const resetSelectors = "blockquote, body, dd, dl, dt, fieldset, figure, h1, h2, h3, h4, h5, h6, hr, html, iframe, legend, li, ol, p, pre, textarea, ul"
.split(", ").reduce((text,e) => `${text}, & ${e}`, "").substring(1);

export const marginBottomSelectors = "blockquote, dl, ol, p, pre, table, ul"
.split(", ").reduce((text,e) => `${text}, & ${e}:not(:last-child)`, "").substring(1);

export default {
    markdown: {
        fontFamily: [
            "BlinkMacSystemFont",
            "-apple-system",
            '"Segoe UI"',
            "Roboto",
            "Oxygen",
            "Ubuntu",
            "Cantarell",
            '"Fira Sans"',
            '"Droid Sans"',
            '"Helvetica Neue"',
            "Helvetica",
            "Arial",
            "sans-serif"
        ].join(","),
        color: theme.text,
        fontSize: "1rem",
        fontWeight: 400,
        lineHeight: 1.5,

        [resetSelectors]: {
            margin: 0,
            padding: 0
        },

        [marginBottomSelectors]: {
            marginBottom: "1em"
        },

        "& h1, & h2, & h3, & h4, & h5, & h6": {
            color: theme.text,
            fontWeight: "600",
            lineHeight: "1.125"
        },

        "& h1": { fontSize: "2em", marginBottom: ".5em" },
        "& h2": { fontSize: "1.75em", marginBottom: ".5714em" },
        "& h3": { fontSize: "1.5em", marginBottom: ".6666em" },
        "& h4": { fontSize: "1.25em", marginBottom: ".8em" },
        "& h5": { fontSize: "1.125em", marginBottom: "..8888em" },
        "& h6": { fontSize: "1em", marginBottom: "1em" },

        "& strong": {
            color: theme.text,
            fontWeight: 700
        },

        "& ul": {
            listStyle: "disc outside",
            marginLeft: "2em",
            marginTop: "1em"
        },

        "& ul ul": {
            marginTop: ".25em"
        },

        "& ol": {
            listStyle: "decimal outside",
            marginLeft: "2em",
            marginTop: "1em"
        },

        "& dd": {
            marginLeft: "2em"
        },

        "& li + li": {
            marginTop: ".25em"
        },

        "& code": {
            fontFamily: "monospace",
            backgroundColor: theme.code,
            color: theme.accent,
            fontSize: ".875em",
            fontWeight: 400,
            padding: ".25em .5em .25em"
        },

        "& a": {
            color: theme.link,
            cursor: "pointer",
            textDecoration: "none"
        },

        "& blockquote": {
            backgroundColor: "#f5f5f5",
            borderLeft: "5px solid #dbdbdb",
            padding: "1.25em 1.5em"
        },

        "& pre, & pre > code": {
            backgroundColor: theme.code,
            color: theme.quote,
            fontSize: ".875em",
            overflowX: "auto",
            padding: "1.25em 1.5em",
            whiteSpace: "pre",
            wordWrap: "normal",
            "-webkit-overflow-scrolling": "touch"
        },

        "& pre > code": { padding: 0 }
    }
}