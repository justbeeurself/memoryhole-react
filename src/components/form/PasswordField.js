import React from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Visibility from "@material-ui/icons/Visibility";

export default class PasswordField extends React.Component {
    state = {
        passwordVisible: false
    }

    onToggleVisibility = () =>
        this.setState(state => ({ passwordVisible: !state.passwordVisible }));

    endAdornment = () => (
        <InputAdornment position="end">
            <IconButton
                aria-label="Toggle password visibility"
                onClick={this.onToggleVisibility}
                onMouseDown={e => e.preventDefault()}>
                {this.state.passwordVisible ? <VisibilityOff /> : <Visibility />}
            </IconButton>
        </InputAdornment>
    )

    render() {
        let { showLabel = false, name, placeholder, inputProps = {}, defaultValue, value, onChange, onFocus, onBlur, ...props } = this.props;
        let { passwordVisible } = this.state;

        return (
            <FormControl {...props}>
                {showLabel && (
                    <InputLabel>{showLabel}</InputLabel>
                )}
                <Input {...inputProps} name={name}
                    placeholder={placeholder}
                    defaultValue={defaultValue}
                    value={value}
                    onChange={onChange}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    type={passwordVisible ? "text" : "password"}
                    endAdornment={this.endAdornment()}
                />
            </FormControl>
        )
    }
}