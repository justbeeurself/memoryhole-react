import React, { Component } from "react";
import PostForm from "./components/IndexForm";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { isGrid } from "~/hoc/MaterialHocs";

import style from "./styles";

@isGrid({ spacing: 8 })
@withStyles(style)
export default class IndexView extends Component {
    render() {
        let { classes } = this.props;

        return (
            <Grid item xs={12} className={classes.topCard}>
                <Grid item md={10} sm={12}>
                    <Paper className={classes.paper}>
                        <PostForm history={this.props.history} />
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}