import settings from "./services/settings";
import { DEFAULT_COLORS } from "memoryhole-common/dist/colors";

const defaultSettings = {
    showHelp: {
        type: "boolean",
        default: true,
        icon: "live_help",
        label: "Show help on startup"
    },
    autoRenderImages: {
        type: "boolean",
        default: true,
        icon: "image",
        label: "Show images automatically"
    },
    parseEmojis: {
        type: "boolean",
        default: true,
        icon: "mood",
        label: "Render emojis"
    },
    defaultIdentity: {
        type: "password",
        default: "",
        icon: "person",
        label: "Default identity"
    },
    narrowPosts: {
        type: "boolean",
        default: false,
        icon: "stay_primary_portrait",
        label: "Create Narrow Posts"
    },
    defaultColors: {
        type: "group",
        label: "Default Post Colors",
        icon: "palette",
        items: [
            {
                type: "color",
                id: "foreground",
                label: "Page Foreground",
                default: DEFAULT_COLORS.foreground
            },
            {
                type: "color",
                id: "background",
                label: "Page Background",
                default: DEFAULT_COLORS.background
            },
            {
                type: "color",
                id: "text",
                label: "Text",
                default: DEFAULT_COLORS.text
            },
            {
                type: "color",
                id: "link",
                label: "Link",
                default: DEFAULT_COLORS.link
            },
            {
                type: "color",
                id: "quote",
                label: "Quote",
                default: DEFAULT_COLORS.quote
            },
            {
                type: "color",
                id: "code",
                label: "Code",
                default: DEFAULT_COLORS.code
            },
            {
                type: "color",
                id: "accent",
                label: "Accent",
                default: DEFAULT_COLORS.accent
            }
        ]
    }
};

settings.loadSettings(defaultSettings, true);
export default defaultSettings;