import "@fortawesome/fontawesome-free/js/all.js";
import "emoji-mart/css/emoji-mart.css";

export default theme => ({
    popover: {
        zIndex: 20000
    },

    editor: {
        "& + .help": {
            marginTop: "-1.5rem",
            marginBottom: "1rem"
        },

        "&.is-errored .editor-toolbar, &.is-errored .CodeMirror": {
            borderColor: "red"
        },

        "&.fullscreen-editor-mode": {
            position: "fixed",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            zIndex: 10000
        },

        "& .editor-toolbar button.active": {
            backgroundColor: "#dedede",
            borderColor: "transparent"
        },

        "& .cm-header": {
            fontWeight: "normal"
        },

        "@media (max-width: 959px)": {
            "& .editor-toolbar, & .CodeMirror": {
                borderRadius: 0,
                borderLeft: 0,
                borderRight: 0
            }
        }
    }
});