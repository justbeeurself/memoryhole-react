import React from "react";
import injectSheet from "react-jss";
import classNames from "classnames";
import AsyncComponent from "~/components/AsyncComponent";
import Slider from "@material-ui/lab/Slider";
import * as colorset from "@material-ui/core/colors";

let accents = [ 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, "A100", "A200", "A400", "A700" ];
let colors = [
    "red", "pink", "purple",
    "deepPurple", "indigo", "blue",
    "lightBlue", "cyan", "teal",
    "green", "lightGreen", "lime",
    "yellow", "amber", "orange",
    "deepOrange", "brown", "grey",
    "blueGrey"
];

const style = {
    slider: {
        marginBottom: "1rem"
    },
    colored: {
        backgroundColor: ({ value = 5 }) =>
            colorset[colors[value]][500]
    },
    subcolored: {
        backgroundColor: ({ set = 5, value = 5 }) =>
            colorset[colors[set]][accents[value]]
    },
    thumb: {
        width: "2rem !important",
        height: "2rem !important",
        border: ".125rem solid #000"
    }
}

@injectSheet(style)
export default class ColorfulSlider extends AsyncComponent {
    getClasses = (classes, isSubcolor) => isSubcolor ? ({
        track: classes.subcolored,
        trackBefore: classes.subcolored,
        thumb: classNames(classes.subcolored, classes.thumb)
    }) : ({
        track: classes.colored,
        trackBefore: classes.colored,
        thumb: classNames(classes.colored, classes.thumb)
    });

    render() {
        let { classes, isSubcolor = false, ...other } = this.props;
        return (
            <Slider {...other} classes={this.getClasses(classes, isSubcolor)} className={classes.slider} />
        );
    }
}