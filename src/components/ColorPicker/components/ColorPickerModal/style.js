export default {
    modal: {
        minWidth: "30rem",
        maxWidth: "100%",

        "@media (max-width: 959px)": {
            minWidth: "100%"
        }
    }
}