import React from "react";
import settings from "~/services/settings";

import { LocalStorageListener } from "~/lib/shims";

export const withSettingsAccess = Component => class extends React.Component {
    state = {
        updateForcer: false,
        settings: {}
    };

    onSettingsUpdate = () => this.setState({ updateForcer: +new Date() });
    componentDidMount = this.onSettingsUpdate;

    render() {
        let { updateForcer } = this.state;
        
        return (
            <React.Fragment>
                <LocalStorageListener onChange={this.onSettingsUpdate} />
                <Component {...this.props}
                    lastSettingsUpdate={updateForcer}
                    settings={settings} />
            </React.Fragment>
        );
    }
}