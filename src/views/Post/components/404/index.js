//Thanks to https://codepen.io/hakimel/pen/fILbu for such a lovely background!

import React from "react";
import { withStyles } from "~/hoc/StyleComposer";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

import style, { numberOfLayers } from "./style";

@withStyles(style)
export default class NotFound extends React.Component {
    render() {
        let { classes } = this.props;

        return (
            <React.Fragment>
                <div className={classes.wrapper}>
                    {Array(numberOfLayers).fill(0).map((_,i) => (
                        <i key={i} className={classes.wave} />
                    ))}
                </div>
                <div className={classes.error}>
                    <Typography variant="display3">Sorry,</Typography>
                    <Typography variant="headline">it appears this page has fallen victim to the memory hole.</Typography>
                    <Typography variant="headline" className={classes.link}>
                        <Link to="/">Home</Link>
                    </Typography>
                </div>
            </React.Fragment>
        )
    }
}