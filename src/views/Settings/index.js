import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { isGrid } from "~/hoc/MaterialHocs";
import SettingsList from "./components/SettingsList";
import style from "./styles";

@isGrid({ spacing: 8 })
@withStyles(style)
export default class SettingsView extends Component {
    render() {
        let { classes } = this.props;

        return (
            <Grid item xs={12} className={classes.topCard}>
                <Grid item lg={5} md={6} sm={9} xs={12}>
                    <Paper className={classes.paper}>
                        <form autoComplete="off">
                            <SettingsList />
                        </form>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}